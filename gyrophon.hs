#!/usr/bin/env runghc

{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

import Control.Monad
import Csound.Base hiding (gain, masterVolume, modes)
import GHC.Float
import qualified Options.Applicative as OP
import System.Exit

safeMlp cutoff = mlp $ clip cutoff 0 22050

data GyMode = Strings [Sig]
            | Beats
            | AM Sig
            | Homophon
            | Cluster [Sig]

type GyInstr = GyParam -> SE Sig

data GyParam = GyParam { f0 :: Sig
               , gain :: Sig
               , effect :: Sig
               , poly :: Sig
               , multiTouch1x :: Sig
               , multiTouch1y :: Sig
               , multiTouch2x :: Sig
               , multiTouch2y :: Sig
               , tuning :: Sig
               }

data UiParam = UiParam { concertPitch :: Sig
                , effectChain :: Fx (Sig, Sig)
                , selectedModes :: [GyParam -> SE Sig]
                , masterVolume :: Sig
                , accumulator :: [SE Sig] -> SE Sig
                , micLevel :: Sig
                }

constant x _ = x


relu x = (x + abs x) / 2

aToF = exp . (* log 2)
-- aToF = (**) 2 . (/12)
-- aToF a = exp (a / 12 * log 2)
fToA = logBase 2
getGain' t'' = (1.5 * t) ^ 2
  where t' = 1 - abs t''
        t  = relu (t' - 0.33)
getGain t ty = t * (1.5 * relu (ty - 0.33) ^ 2)
getEff tx = on 0.01 0.99 (1 - tx)


gyrophonParam :: OscRef -> String -> SE GyParam
gyrophonParam oscRef idf = do
  let a ""  = "/gyaccelerometer"
      a idf = "/" ++ idf ++ a ""
      t n "" = "/touch" ++ show n
      t n idf =  "/" ++ idf ++ t n ""

  (a0, a1)         <- listenOscVal oscRef (a idf) (sig 0, sig 0)
  (t0, t0x, t0y)   <- listenOscVal oscRef (t 0 idf) (sig 0, sig 0.5, sig 0.5)
  (t1, t1x, t1y)   <- listenOscVal oscRef (t 1 idf) (sig 0, sig 0.5, sig 0.5)
  (t2, t2x, t2y)   <- listenOscVal oscRef (t 2 idf) (sig 0, sig 0.5, sig 0.5)

  return GyParam {
      f0=smooth 0.01 (aToF a1)
    , gain=smooth 0.01 (getGain t0 t0y)
    , effect=smooth 0.01 (getEff t0x)
    , poly=smooth 0.01 a0
    , multiTouch1x=smooth 0.01 t1x
    , multiTouch1y=smooth 0.01 t1y
    , multiTouch2x=smooth 0.01 t2x
    , multiTouch2y=smooth 0.01 t2y
    , tuning=1
    }


setTuning :: Sig -> GyParam -> GyParam
setTuning tuning param = param{f0=f0 param * tuning, tuning=tuning}


------------ Instruments -----------------------------------------------
oscInstr :: (Sig -> Sig) -> GyInstr
oscInstr osc' GyParam{f0, gain, effect}
  = return $ fltr $ gain * osc' f0
  where
    fltr = at (safeMlp cutoff 0.1)
    cutoff = 5 * f0 * (effect ** 1.5)

filterInstr :: Sig -> GyInstr
filterInstr source GyParam{f0=f0, gain=gain, effect=effect} = return $ gain' *
  at filtre source
    where bandwidth = f0 * log effect ^ 2 * 5
          gain'     = gain / (1 - effect)
          filtre    = filt 2 bbp f0 bandwidth


------------ Play modes ------------------------------------------------
gyrophonBeats :: GyInstr -> GyParam -> SE Sig
gyrophonBeats instr param = do
  upper <- instr param
  lower <- instr param{f0=f0 param * aToF (poly param / 2)}
  return $ upper + lower

gyrophonAM :: Sig -> GyInstr -> GyParam -> SE Sig
gyrophonAM oscTuning instr param = do
  voice <- instr param
  let oscillator = osc $ oscTuning * tuning param * aToF (poly param)
  return $ oscillator * voice

strings = [-3, -2, -1, 0, 1, 2, 3]

gyrophonStrings :: [Sig] -> GyInstr -> GyParam -> SE Sig
gyrophonStrings strings instr param = sum $ fmap gyString strings
  where
    gyString n = instr param{
      f0=f0 param * (n + 4) / 4,
      gain=gain param * gyPoly ((3 * poly param - n))
    }
    gyPoly p = abs (p + 5/6) - abs (p + 1/2) - abs (p - 1/2) + abs (p - 5/6)

clusterVoices = [1, 2, 3, 4, 5, 6, 7, 8]

gyrophonCluster :: [Sig] -> GyInstr -> GyParam -> SE Sig
gyrophonCluster voices instr param = sum $ fmap gyCluster voices
  where
    gyCluster n = instr param{
      f0=aToF (poly param * n / 4) * f0 param,
      gain=gain param * 100 / (n + 10) / (n + 10)
    }

gyrophonHomophon :: GyInstr -> GyParam -> SE Sig
gyrophonHomophon = id

runMode :: GyMode -> GyInstr -> GyParam -> SE Sig
runMode (Strings strings) = gyrophonStrings strings
runMode Beats             = gyrophonBeats
runMode (AM oscTuning)    = gyrophonAM oscTuning
runMode (Cluster voices)  = gyrophonCluster voices
runMode Homophon          = gyrophonHomophon

modes = [ Strings strings
        , Beats
        , AM 1
        , Cluster clusterVoices
        , Homophon ]


------------ GUI -------------------------------------------------------
titleString :: Show a => a -> [Char]
titleString port = "Gyrophon on port " ++ show port
  ++ " v" ++ versionId ++ " ("  ++ website ++ ")"

basicGui :: Show a => Double -> [String] -> a -> Sig -> SE UiParam
basicGui cPitch ids port mic = do
  (gTuning, setTuning, tuning) <- setSlider "tuning" (expSpan 15 44100) cPitch
  gVersion                     <- box $ titleString port
  (gGyrophons, modes)          <- unzip <$> mapM (`gyrophonWidget` mic) ids
  (gMic, micLevel)             <- micWidget
  (gMaster, masterLevel)       <- volumeWidget "Master" False
  (gFx, fx)                    <- effectWidget
  gAccLabel                    <- box "Acc"
  (gAccumulator, acc)          <- vinstrChooser [ ("+", sum)
                                                , ("* mic", product)
                                                , ("*", productWithoutMic)
                                                ] 0

  keyPanel $ -- "Gyrophon" (600, 300) $
    ver [ sca 0.1 $ setFontSize 32 $ setEmphasis Bold gVersion
        , sca 0.1 gTuning
        , hor (
          [ sca 0.16 gMaster
          , sca 0.16 gMic
          , sca 0.16 $ ver [space, sca 0.1 gAccLabel, sca 2 gAccumulator, space]
          ] ++ gGyrophons
            ++ [ sca (int2Double (4 - length gGyrophons)) space ]
          )
        , gFx]

  let tuning' = 440 * exp $ (*) (log 2 / 12) $ evtToSig 0 $ appendE 0 (+)
        $  devt (-12) (charOn 'q')
        <> devt 12 (charOn 'w')
        <> devt (-7) (charOn 'e')
        <> devt 7 (charOn 'r')
        <> devt (-1) (charOn 'u')
        <> devt 1 (charOn 'i')
  setTuning tuning'

  return UiParam{
        concertPitch=tuning
      , effectChain=fx
      , selectedModes=modes
      , accumulator=acc
      , micLevel=micLevel
      , masterVolume=masterLevel
    }

gyrophonWidget :: String -> Sig -> Source GyInstr
gyrophonWidget idf mic = do
  gModesLabel           <- box "Play mode"
  gSynthLabel           <- box "Synthesizer"
  gEffectLabel          <- box "Effects"
  gIdLabel              <- box $ if idf == "" then "Gyrophon" else idf
  (gVolume, volume)     <- volumeWidget "Level" False

  nz <- white
  (gInstrChoser, instr) <- vinstrChooser
    [ ("Saw", oscInstr saw)
    , ("Sqr", oscInstr sqr)
    , ("Noise", filterInstr nz)
    , ("Mic", filterInstr mic)
    , ("Sin", oscInstr osc)
    ] 0

  (gModeChoser, mode)   <- vinstrChooser
    [ ("Strings", runMode (Strings strings) instr)
    , ("Beats", runMode Beats instr)
    , ("AM", runMode (AM 1) instr)
    , ("Cluster", runMode (Cluster clusterVoices) instr)
    , ("Homophonic", runMode Homophon instr)
    ] 0

  (gAmToggle1, am1)     <- toggle "AM 440" False
  (gAmToggle2, am2)     <- toggle "AM 220" False
  (gAmToggle3, am3)     <- toggle "AM 880" False

  let idLabel = setEmphasis (if idf == "" then Italic else Bold) gIdLabel
      widget = setBorder UpBoxBorder $
               ver [sca 0.1 $ setFontSize 28 idLabel
                   , hor [ setBorder ThinDown $ ver [sca 0.2 gModesLabel , sca 5 gModeChoser]
                         , setBorder ThinDown $ ver [sca 0.2 gSynthLabel, sca 5 gInstrChoser]
                         , setBorder ThinDown $ ver [sca 0.2 gEffectLabel, gAmToggle1, gAmToggle2, gAmToggle3]
                         , sca 0.5 gVolume
                         ]
                   ]
  return (widget, at (* volume)
                . at (amEffect 440 am1)
                . at (amEffect 220 am2)
                . at (amEffect 880 am3)
                . mode)

amEffect f amEvt = (* amSource)
  where amSource = cfd (evtToSig 0 amEvt) 1 (osc f)

effectWidget = fxHor [ uiFilter False 0.5 0.5 0.5
                     , uiChorus False 0.5 0.5 0.5 0.5
                     , uiPhaser False 0.5 0.5 0.5 0.5
                     , uiDistort False 0.5 0.9 0.5
                     , uiDelay False 0.5 0.5 0.1 0.3
                     , uiRoom 0.2 ]

micWidget = volumeWidget "Mic IN" True

volumeWidget label initialMute = do
  (gMicSlider, micInvLevel) <- slider label (expSpan 1e-1 1e5) 1
  (gMicMute, micMute)       <- toggle "mute" initialMute
  let widget = ver [setOrient Ver gMicSlider, sca 0.1 gMicMute]
      muteLevel = 1 - evtToSig 0 micMute
  return (widget, muteLevel / micInvLevel)

productWithoutMic [] = 1
productWithoutMic (mic:gyrophones) = product gyrophones

performN :: Monad m => Int -> m t -> m [t]
performN 0 _ = return []
performN n m = do
  m' <- m
  ms <- performN (n-1) m
  return (m':ms)


dbMeter signal = (-20) * logBase 10 envelope
  where ksig = kr signal
        envelope = smooth 0.1 $ abs ksig

cabbageGui :: Sig -> SE UiParam
cabbageGui = undefined

serverModeUI :: OscRef -> Sig -> SE UiParam
serverModeUI oscRef _ = do
  -- TODO: Implement
  --               , effectChain
  --               , mode selection
  --               , accumulator
  tuning      <- listenOscVal oscRef "tuning" $ sig 0
  micLevel    <- listenOscVal oscRef "micLevel" $ sig 0
  masterLevel <- listenOscVal oscRef "masterLevel" $ sig 0
  return UiParam{
        concertPitch=tuning
      , effectChain=return
      , selectedModes=[runMode (Strings strings) (oscInstr saw)]
      , accumulator=sum
      , micLevel=micLevel
      , masterVolume=masterLevel
  }


------------ CLI -------------------------------------------------------

data CliOptions = CliOptions
  { cliVersion :: Bool
  , cliPort :: Int
  , cliCsdOutput :: String
  , cliConcertPitch :: Double
  , cliUseCabbage :: Bool
  , cliServerMode :: Bool
  , cliDeviceIds :: [String]
  }

versionId :: String
versionId = "1.0"
license :: String
license = "GNU GPLv3"
author :: String
author = "Frederik Bous"
authorEmail :: String
authorEmail = "gyrophon@fnab.xyz"
website :: String
website = "https://gyrophon.fnab.xyz"
versionStr :: String
versionStr = "Gyrophon "
           ++ "version " ++ versionId
           ++ ", author " ++ author ++ " (" ++ authorEmail ++ "), "
           ++ "license " ++ license
version :: IO ()
version = putStrLn versionStr
exit :: IO ()
exit = exitSuccess

cliVersionFlag :: OP.Parser Bool
cliVersionFlag = OP.flag False True
  (OP.long "version" <> OP.short 'V' <> OP.help "Show version info and exit")

cliCsdOutputOption :: OP.Parser String
cliCsdOutputOption = OP.strOption
  (OP.long "output-file" <> OP.short 'o' <> OP.metavar "OUTPUT-FILE"
   <> OP.value "" <> OP.help "Write to csound file without launching")

cliConcertPitchOption :: OP.Parser Double
cliConcertPitchOption = OP.option OP.auto
  (OP.long "concert" <> OP.short 'k' <> OP.metavar "CONCERT_PITCH"
   <> OP.value 440 <> OP.help "Preselect concert pitch (Default 440)")

cliPortOption :: OP.Parser Int
cliPortOption = OP.option OP.auto
  (OP.long "port" <> OP.short 'p' <> OP.metavar "PORT" <> OP.value 9000
   <> OP.help "Port for receiving osc data")

cliDeviceIdentifiers :: OP.Parser [String]
cliDeviceIdentifiers = OP.many $ OP.strOption
  (OP.long "ids" <> OP.short 'i' <> OP.metavar "ID"
   <> OP.help
      ("Identifier for distingushing multiple devices. "
       ++ "Specify multiple ids to allow multiple input devices. "
       ++ "(Default none)"))

cliUseCabbageOption :: OP.Parser Bool
cliUseCabbageOption = OP.flag False True
  (OP.long "cabbage" <> OP.short 'c' <> OP.help "Use cabbage user interface")

cliServerModeOption :: OP.Parser Bool
cliServerModeOption = OP.flag False True
  (OP.long "server" <> OP.short 's'
   <> OP.help "Server Mode: No graphical UI but listen for osc messages instead")

cliParseOptions :: OP.Parser CliOptions
cliParseOptions = CliOptions
  <$> cliVersionFlag
  <*> cliPortOption
  <*> cliCsdOutputOption
  <*> cliConcertPitchOption
  <*> cliUseCabbageOption
  <*> cliServerModeOption
  <*> cliDeviceIdentifiers

getCliOptions :: IO CliOptions
getCliOptions = OP.execParser $ OP.info ( cliParseOptions <**> OP.helper )
               ( OP.fullDesc
                <> OP.progDesc "Play a melody with your cell phone"
                <> OP.header "Gyrophon" )

notEmpty "" = False
notEmpty _  = True


------------ Main ------------------------------------------------------

main :: IO ()
main = do
  CliOptions{..} <- getCliOptions
  when cliVersion (version >> exit)
  let oscRef = initOsc cliPort
      ui | cliUseCabbage = cabbageGui
         | cliServerMode = serverModeUI oscRef
         | otherwise     = basicGui cliConcertPitch cliDeviceIds cliPort
      program = csoundProgram cliDeviceIds oscRef ui
  when (notEmpty cliCsdOutput) $ writeCsd cliCsdOutput program >> exit
  dac program


csoundProgram :: [String] -> OscRef -> (Sig -> SE UiParam) -> (Sig -> SE Sig2)
csoundProgram idfs oscRef ui mic = do
  uiParam <- ui mic
  synthesisChain idfs oscRef uiParam mic

synthesisChain :: [String] -> OscRef -> UiParam -> (Sig -> SE Sig2)
synthesisChain idfs oscRef UiParam{..} mic =
  at (* masterVolume)
     . at effectChain
     . accumulator
     . ((return $ mic * micLevel) :)
     . zipWith ($) selectedModes
    =<< mapM (fmap (setTuning concertPitch) . gyrophonParam oscRef) idfs
