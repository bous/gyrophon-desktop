# Gyrophon synthesis server

## How it works
The quickest way to play is using only [csound](https://csound.com/)
and the [Gyrophon App](https://gitlab.com/bous/gyrophon-android).

If you have installed csound on your pc,
the app on your mobile phone
and cloned this repository
we are good to go.

1. Make sure both your cell phone and your pc
   are connected to the same network
   and are visible to each other.
   You may want to ping between them, when in doubt.
1. Find out your pcs local ip address.
   This is typically something like
   `192.168.x.y`,
   where you need to figure out `x` and `y`
   To find your local ip address the command
   `ifconfig` (or `ipconfig` on Windows) will help
   look for the entry `inet`.
1. Start up the synthesizer with
   ```bash
   csound gyrophon.csd
   ```
   A control panel should appear.
1. In the mobile app press the three dots in the top corner,
   click "Settings"
   and enter the following settings:
   | Option    | Value                 |
   | ------    | -----                 |
   | Host      | *your pcs ip address* |
   | Port      | 9000                  |
   | Device Id | *Not set*             |
   Go back to the main screen
   when you have entered the settings.
1. On the main screen touch the upper third of the grid
   and move your finger down while touching the screen.
   Make sure your pc is not on mute.


If you have trouble setting up a connection
please contact me at `gyrophon@fnab.xyz`.


### Launching with `runghc`
If you have installed haskell with this projects dependencies
you can run the source file directly.

Make sure the source file is executable
```bash
chmod u+x gyrophon.hs
```
Replace the command in step 3 with
```bash
./gyrophon.hs
```
To see additional options run
```bash
./gyrophon.hs --help
```


## Install

### The app
You need the [app](https://gitlab.com/bous/gyrophon-android) installed on a mobile phone.
Please follow the instructions
[here](https://gitlab.com/bous/gyrophon-android).

### CSound
In any case, you need to have
[csound](https://csound.com/) installed.

#### Windows
For windows you can use the installer
provided by the
[csound download page](https://csound.com/download.html).
* Unless you have a **very** old computer,
  select the *64bit Binaries* version for Windows

#### Linux
Most distros have csound included in the repositories.
For Debian/Ubuntu/Mint do
```bash
sudo apt install csound
```
The debian repositories are not quite up to date.
If you encounter problems in the next steps,
it is worth trying to install the latest version of csound
from source but it should not be neccessary.

### Haskell
To fully hack into your instrument
you need to install haskell
and the
[csound-expression](https://github.com/spell-music/csound-expression)
library.

I'm using GHC 8.8.4
but any modern Haskell compiler should do.
To install ghc I recommend [ghcup](https://www.haskell.org/ghcup/).
From the Website:
```bash
curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh
# Follow the instructions presented by the installer
# in case of errors consult https://www.haskell.org/ghcup/
```

If you have ghc installed with ghcup,
you should also have cabal.
We can use cabal
to install all dependencies.
The following commands need to be executed in the project:
```bash
cabal update
cabal install csound-expression optparse
```



## Playing

This app only reads the sensor data in your mobile device,
displays them
and sends them to a PC where it is used
to actually synthesize the sound.
This is because Gyrophon is intended to be used
with proper audio hardware
and not through your phones speaker
(although I'm planing to implement a portable version
 sometime soon, see TODO).
